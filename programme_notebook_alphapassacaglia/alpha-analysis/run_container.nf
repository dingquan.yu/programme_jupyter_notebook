nextflow.enable.nextflow.enable.dsl=2

params.results_dir = ''
params.cutoff=10
params.create_notebook='True'

process get_good_pae{
    container 'ghcr.io/henrywotton/programme_jupyter_notebook:0.2'
    executor 'slurm'
    memory "32GB"
    queue 'htc'

    input:
        path results_dir
        val cutoff
        val create_notebook

    script:
    """
    /app/run_get_good_pae.sh --output_dir=${results_dir} --cutoff=${cutoff} --create_notebook=${create_notebook}
    """
}

workflow {
  get_good_pae(params.results_dir,params.cutoff,params.create_notebook)
}